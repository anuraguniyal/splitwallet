package com.agyey.splitwallet.controls;


import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditBTCAddress extends EditText {

	private class CustomTextWatcher implements TextWatcher {
	    private EditBTCAddress mEditAddr;

		public CustomTextWatcher(EditBTCAddress e) {
			mEditAddr = e;
		}
	
		public void beforeTextChanged(CharSequence s, int start, int count,
		        int after) {
		}
	
		public void onTextChanged(CharSequence s, int start, int before,
		        int count) {
	
		}
	
		public void afterTextChanged(Editable s) {
			mEditAddr.validate();
		}
	}

	public EditBTCAddress(Context context) {
		super(context);
		init();
	}

	public EditBTCAddress(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public EditBTCAddress(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		addTextChangedListener(new CustomTextWatcher(this));
	}
	
	protected void validate() {
		this.setError("Not a valid bitcoin address");
	}
}
